# Grupo 2

## Integrantes:
* Júlio Sousa (Líder)
* Magnun (Programador - JAVA)
* Wilian (Programador - Ruby)
* José Umberto (Programador - Javascript)
* Yan (Documentador)

# **Jogo**
Parole

# **Quantos jogadores?**
2 jogadores por sala.

# **Como se joga?**
O jogo é multiusuário, onde são criadas salas de 2 jogadores por vez. O sistema gera uma matriz 4x4 que é mostrada para cada jogador. Ela é preenchida randomicamente com letras do alfabeto. O jogador deve encontrar palavras ligando letras adjacentes, ou seja, ele pode utilizar apenas letras que estejam ao lado da letra escolhida. Dessa forma, ele deve formar palavras com pelo menos 3 letras que existam na língua portuguesa. Cada letra da palavra certa significa 1 ponto a mais para o usuário (Ex: "você": 4 letras = 4 pontos), que terá 3 minutos para resolver e encontrar o máximo de palavras possíveis.

# **Final do jogo**
Passado o tempo, o sistema mostra um relatório para os dois jogadores com as pontuações dele e do adversário e o resultado da disputa. O relatório terá os seguintes dados:
* Cada palavra digitada pelos jogadores e seu status: **válida** ou **inválida**
* Palavras _válidas_ são palavras que respeitam a regra do jogo (formar palavras apenas com letras adjacentes).
* A **pontuação total** de cada jogador e uma mensagem informando o vencedor.
    
# **Como se ganha?**
Ganha quem, ao final da batalha, fizer mais pontos.
